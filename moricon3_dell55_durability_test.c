
#include <xc.h>
#define _XTAL_FREQ 8000000
#pragma config BOREN = OFF
#pragma config FOSC = INTRC_NOCLKOUT
#pragma config FCMEN = OFF
#pragma config MCLRE = OFF
#pragma config WDTE = OFF
#pragma config LVP = OFF
#pragma config PWRTE = ON

#define timerH cf
#define timerL 2c
#define NormalCounter 4                               //normal drive time counter
#define ReversalCounter 4                             //reversal drive time counter
#define IntervalCounter 10                            //interval time counter
#define StopCounter 3                                 //stop counter

int mode;                                             //mode
    //mode 0:stop
    //mode 1:operating
    //mode 2:irregular stop
int st;                                               //state flag
    //st 1:normal drive
    //st 2:normal drive -> interval
    //st 3:reversal drive
    //st 4:reversal dribe -> interval
int nc;                                               //normal drive time counter
int ic;                                               //interval time counter
int rc;                                               //reversal drive time counter
int dc;                                               //drive comand counter
int mc;                                               //MSW counter
int sc;                                               //stop counter
int RB4bfr;                                           //MSW before


/*main setting*/
void main(void)
{
////input////
//RB0:change sw
//RB1:start sw
//RB2:stop sw
//RB3:stop signal input
//RB4:MSW input

////output////
//RA3:MSW count output
//RA6:blue LED
//RC0:power LED
//RC1:yellow LED
//RC2:greem LED
//RC3:red LED
//RC4:drive count output
//RC5:driver in2
//RC6:driver in1

OSCCON=0b01110000;                                          //clock 8MHz
CM1CON0=0b00000000;                                         //comparator OFF
PORTA=0b00000000;                                           //portA output 0
PORTB=0b00000000;                                           //portB output 0
PORTC=0b00000000;                                           //portC output 0
TRISA=0b00000000;                                           //portA input
TRISB=0b00011111;                                           //portB input RB0,1,2,3,4
TRISC=0b00000000;                                           //portC input -

ANSEL=0b00000000;                                           //nothing analog
ANSELH=0b00000000;                                          //nothing analog

WPUB=0b00011111;                                            //RB0,1,2,3,4 internal pull up
OPTION_REG=0b00000000;                                      //pull up enable

/*timer interrupt 1 setting*/
T1CON=0x30;                                                 //interval timer
TMR1H=0xtimerH;                                             //timer H
TMR1L=0xtimerL;                                             //timer L
TMR1IE=1;                                                   //interrupt ok
TMR1ON=1;                                                   //timer1 start

PEIE=1;                                                     //all interrupt ok
GIE=1;                                                      //all interrupt ok

/*default*/
mode=0;                                                     //mode = stop
st=0;                                                       //state = standby
nc=0;                                                       //normal drive time counter = 0
rc=0;                                                       //reversal drive time counter = 0
ic=0;                                                       //interval time counter = 0
dc=0;                                                       //drive comand counter = 0
mc=0;                                                       //MSW counter = 0
sc=0;                                                       //stop counter = 0
//RB4bfr=1;                                                 //MSW before = OFF

///////////////////////main loop//////////////////////////
while(1)                                                    //main loop
    {
    RC0=1;                                                  //power LED ON
    }
}
/////////////////////timer interrupt//////////////////////
void interrupt A(void)
{
    if(TMR1IF)
        {
        TMR1H=0xtimerH;                                     //timer H
        TMR1L=0xtimerL;                                     //timer L
        }
////////manual drive///////
////////MSW count////////
if(RB0==0)                                                  //change sw ON
    {
    if(RB4==0)                                              //MSW ON
        {
        RC1=1;                                              //yellow LED ON
        }
    else
        {
        RC1=0;                                              //yellow LED OFF
        }
    RB4bfr=RB4;                                             //RB4bfr -> renew
    ////////SOL drive////////
    if(RB1==0)                                              //if start sw ON
        {
        RC6=1;RC5=0;                                        //SOL normal drive
        RC2=1;                                              //green LED ON
        nc=0;ic=0;rc=0;dc=0;mc=0;sc=0;                      //all count reset
        }
    else
        {
        if(RB2==0)                                          //if stop sw ON
            {
            RC6=0;RC5=1;                                    //SOL reversal drive
            RA6=1;                                          //blue LED ON
            nc=0;ic=0;rc=0;dc=0;mc=0;sc=0;                  //all count reset
            }
        else
            {
            RC6=0;RC5=0;                                    //SOL drive stop
            RC2=0;RA6=0;                                    //green,blue LED OFF
            nc=0;ic=0;rc=0;dc=0;mc=0;sc=0;                  //all count reset
            }
        }
    }
else
    {
////////auto drive////////
    ////////MSW count////////
    if(RB4==0)                                              //MSW ON
        {
        RC1=1;                                              //yellow LED ON
        if(RB4bfr==1 && RB4==0)                             //if normal sw OFF->ON
            {
            if(mode==1)                                     //if operating
                {
                mc++;                                       //MSW count +1
                }
            RA3=1;                                          //MSW count output ON
            }
        else
            {
            RA3=0;                                          //MSW count output OFF
            }
        }
    else
        {
        RC1=0;                                              //yellow LED OFF
        if(RB4bfr==0 && RB4==1)                             //if normal sw ON->OFF
            {
            if(mode==1)                                     //if operating
                {
                mc++;                                       //MSW count +1
                }
            RA3=0;                                          //MSW count output OFF
            }
        else
            {
            RA3=0;                                          //MSW count output OFF
            }
        }
        RB4bfr=RB4;                                         //RB4bfr -> renew
    ////////SOL drive////////
    switch(mode)                                            //switch(mode)
        {
        ////////case stop///////
        case 0:                                             //case stop
            if(RB1==0)                                      //if start sw ON
                {
                mode=1;                                     //mode = operating
                if(RB4==1)                                  //if MSW OFF
                    {
                    st=1;                                   //state = normal drive
                    }
                else
                    {
                    st=3;                                   //state = reversal drive
                    }
                }
        break;                                              //case stop break
        //////case operating//////
        case 1:                                             //case operating
            if(dc-mc<=1)                                    //if count no problem
                {
                if(RB3==1)                                  //if stop signal OFF
                    {
                    if(RB2==1)                              //if stop sw OFF
                        {
                        switch(st)                          //switch(state)
                            {
                            case 1:                         //if normal drive
                                if(nc>=NormalCounter)       //if normal drive time count >= 200ms
                                    {
                                    RC6=0;RC5=0;            //SOL drive stop
                                    RC2=0;                  //green LED OFF
                                    RC4=0;                  //drive count output OFF
                                    //RC3=0;                  //red LED OFF(debag)
                                    nc=0;                   //normal drive time count reset
                                    //if(RB4==0)              //if MSW ON
                                    //    {
                                    //    mc++;dc++;          //msw count drive comand count +1
                                    //    st=2;               //state = normal -> interval
                                    //    }
                                    //else
                                    //    {
                                    //    dc++;               //msw count drive comand count +1
                                    //    st=2;               //state = normal -> interval
                                    //    }
                                    //dc++;                     //drive comand count +1
                                    st=2;                     //state = normal -> interval
                                    }
                                else
                                    {
                                    RC6=1;RC5=0;            //SOL normal drive
                                    RC2=1;                  //green LED ON
                                    RC4=0;                  //drive count output OFF
                                    //RC3=1;                  //red LED ON(debag)
                                    nc++;                   //normal drive time count +1
                                    if(nc==1)               //if first count
                                        {
                                        dc++;              //drive comand count +1
                                        RC4=1;             //drive count output ON
                                        }
                                    else
                                        {
                                        RC4=0;             //drive count output OFF
                                        }
                                    }
                            break;                          //case 1 break
                            case 2:                         //if normal -> interval
                                if(ic>=IntervalCounter)     //if interval time count >=1000ms
                                    {
                                    RC6=0;RC5=0;            //SOL drive stop
                                    RC4=0;                  //drive count output OFF
                                    ic=0;                   //interval time count reset
                                    st=3;                   //state = reversal drive
                                    }
                                else
                                    {
                                    RC6=0;RC5=0;            //SOL drive stop
                                    RC4=0;                  //drive count output OFF
                                    ic++;                   //interval time count +1
                                    }
                            break;                          //case 2 break
                            case 3:                         //if reversal
                                if(rc>=ReversalCounter)     //if interval time count >=1000ms
                                    {
                                    RC6=0;RC5=0;            //SOL drive stop
                                    RA6=0;                  //blue LED OFF
                                    RC4=0;                  //drive count output OFF
                                    //RC3=0;                  //red LED OFF(debag)
                                    rc=0;                   //reversal drive time count reset
                                    //if(RB4==1)              //if MSW OFF
                                    //    {
                                    //    mc++;dc++;          //msw count drive comand count +1
                                    //    st=4;               //state = reversal -> interval
                                    //    }
                                    //else
                                    //    {
                                    //    dc++;               //msw count drive comand count +1
                                    //    st=4;               //state = reversal -> interval
                                    //    }
                                    //dc++;                     //drive comand count +1
                                    st=4;                     //state = reversal -> interval
                                    }
                                else
                                    {
                                    RC6=0;RC5=1;            //SOL reversal drive
                                    RA6=1;                  //blue LED ON
                                    RC4=0;                  //drive count output OFF
                                    //RC3=1;                  //red LED ON(debag)
                                    rc++;                   //reversal drive time count +1
                                    if(rc==1)               //if first count
                                        {
                                        dc++;               //drive comand count +1
                                        }
                                    }
                            break;                          //case 2 break
                            case 4:                         //if normal -> interval
                                if(ic>=IntervalCounter)                  //if interval time count >=1000ms
                                    {
                                    RC6=0;RC5=0;            //SOL drive stop
                                    RC4=0;                  //drive count output OFF
                                    ic=0;                   //interval time count reset
                                    st=1;                   //state = normal drive
                                    }
                                else
                                    {
                                    RC6=0;RC5=0;            //SOL drive stop
                                    RC4=0;                  //drive count output OFF
                                    ic++;                   //interval time count +1
                                    }
                            break;                          //case 2 break
                        default:
                            break;                          //st other break
                            }
                    }
        //////manual stop//////
                else
                    {
                    RC6=0;RC5=0;                            //SOL drive stop
                    RA6=0;RC2=0;                            //blue,green LED
                    RC4=0;                                  //drive count output OFF
                    RA3=0;                                  //MSW count output OFF
                    mode=0;                                 //mode = stop
                    nc=0;ic=0;rc=0;dc=0;mc=0;sc=0;          //all count reset
                    st=0;                                   //state = standby
                    }
                }
        //////auto stop//////
            else
                {
                if(sc>=StopCounter)                         //if stop count >= 150ms
                    {
                    RC6=0;RC5=0;                            //SOL drive stop
                    RA6=0;RC2=0;                            //blue,green LED
                    RC4=0;                                  //drive count output OFF
                    RA3=0;                                  //MSW count output OFF
                    mode=0;                                 //mode = stop
                    nc=0;ic=0;rc=0;dc=0;mc=0;sc=0;          //all count reset
                    st=0;                                   //state = standby
                    }
                else
                    {
                    if(st==1)                               //if SOL normal drive
                        {
                        RC6=1;RC5=0;                        //SOL normal drive
                        RC2=1;                              //green LED ON
                        nc=0;                               //normal drive time count reset
                        sc++;                               //stop count +1
                        }
                    else
                        {
                        if(st==3)                           //if SOL reversal drive
                            {
                            RC6=0;RC5=1;                    //SOL reversal drive
                            RA6=1;                          //blue LED ON
                            nc=0;                           //normal drive time count reset
                            sc++;                           //stop count +1
                            }
                        else
                            {
                            sc++;                           //stop count +1
                            }
                        }
                    }
                }
            }
        //////ireegular stop//////
        else
            {
            RC6=0;RC5=0;                                //SOL drive stop
            RA6=0;RC2=0;                                //blue,green LED
            RC3=1;                                      //red LED ON
            RC4=0;                                      //drive count output OFF
            RA3=0;                                      //MSW count output OFF
            mode=2;                                     //mode = irregular stop
            nc=0;ic=0;rc=0;dc=0;mc=0;sc=0;              //all count reset
            st=0;                                       //state = standby
            }
        break;                                          //case operating break
        //////case irregular stop//////
        case 2:                                         //case irregular stop
            RC6=0;RC5=0;                                //SOL drive stop
            RA6=0;RC2=0;                                //blue,green LED
            RC3=1;                                      //red LED ON
            RC4=0;                                      //drive count output OFF
            RA3=0;                                      //MSW count output OFF
            mode=2;                                     //mode = irregular stop
            nc=0;ic=0;rc=0;dc=0;mc=0;sc=0;              //all count reset
            st=0;
        break;                                          //case irregular stop break
        }
    }
TMR1IF=0;                                               //timer1 flag clear
}
